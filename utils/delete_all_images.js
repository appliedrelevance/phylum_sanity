const sanityClient = require('@sanity/client');

const client = sanityClient({
  projectId: 'xc5dwdxg',
  dataset: 'production',
  apiVersion: '2021-06-04',
  token:
    'skV6zrneTdXW7h97oUTP5TD7U8psuHkfTCwYGXshUJCUX04NaSVaPFZsaBVFIZkCwwiVH7iBEuBaHJEZAr3Zrx9q0wJTYj9RWMCk35AgTFYJb0MkNWQ0j7wIrJDGUi5eU2Ej26de3sABvnFn2lV2d2InMnYTBYThZv8iZYhh4lbvYbOPWdyP',
  useCdn: false,
});

const query = `*[_type == "sanity.imageAsset"]{_id}`;

client
  .fetch(query)
  .then((res) => {
    console.log(res);
    return res.map((image) => image._id);
  })
  .then((res) => {
    console.log(res);
    return res.map((id) => {
      client
        .delete(id)
        .then((res) => console.log(`deleted ${res}`))
        .catch((err) => console.error(`Error deleting ${err}`));
    });
  })
  .then((res) => {
    console.log(res);
  })
  .catch((err) => {
    console.error(err);
  });
