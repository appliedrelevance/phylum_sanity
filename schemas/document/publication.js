export default {
  name: 'publication',
  title: 'Publication',
  type: 'document',
  fields: [
    {
      name: 'name',
      title: 'Publication Name',
      type: 'string',
    },
    {
      name: 'title',
      title: 'Publication Title',
      type: 'string',
      },
      {
          name: 'subtitle',
          title: 'Publication Subtitle',

      },
      
    {
      name: 'copyright',
      title: 'Copyright Information',
      type: 'object',
      fields: [
        {
          name: 'firstYear',
          title: 'Year of first publication.',
          type: 'date',
        },
        {
          name: 'currentYear',
          title: 'Year of current publication.',
          type: 'date',
          },
          {
              name: 'holder',
              title: 'Copyright Holders',
              type: 'array',
              of: [
                  { type: 'reference', to: 'organization' },
                  { type: 'reference', to: 'person'}
              ]

        }
      ],
    },
  ],
};
