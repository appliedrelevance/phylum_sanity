export default {
  name: 'gruntfund',
  type: 'document',
  title: 'Grunt Fund',
  fields: [
    {
      name: 'title',
      title: 'Grunt Fund Title',
      type: 'string',
    },
    {
      name: 'grunts',
      title: 'Grunts (members)',
      type: 'array',
      of: [{ type: 'grunt' }],
    },
  ],
};
