export default {
  name: 'grunt',
  type: 'object',
  title: 'Grunt',
  description: 'Slicing Pie "Grunt" is a member of a "Grunt Fund"',
  fields: [
    {
      name: 'profile',
      type: 'reference',
      to: [{ type: 'person' }],
      title: 'Grunt Profile',
    },
    { name: 'ghrr', type: 'number', title: 'Grunt Hourly Resource Rate' },
  ],
};
