export default {
  name: 'taxonomy',
  type: 'document',
  title: 'Taxonomy',
  fields: [
    { name: 'name', title: 'Taxonomy Name', type: 'string' },
    { name: 'description', title: 'Taxonomy Description', type: 'text' },
    { name: 'root', type: 'reference', to: [{ type: 'concept' }] },
  ],
};
