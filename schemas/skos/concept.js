export default {
  name: 'concept',
  type: 'object',
  title: 'Concept',
  fields: [
    { name: 'prefLabel', type: 'string', title: 'Preferred Label' },
    { name: 'note', type: 'text', title: 'Concept Note' },
    { name: 'rule', type: 'text', title: 'Classification Rule' },
    { name: 'grammar', type: 'string', title: 'Classification Grammar' },
    {
      name: 'altLabels',
      type: 'array',
      title: 'Alternate Labels',
      of: [{ type: 'string' }],
    },
    {
      name: 'topConcept',
      type: 'reference',
      to: [{ type: 'concept' }],
      title: 'Top Concept',
    },
    {
      name: 'broader',
      type: 'array',
      of: [{ type: 'reference', to: [{ type: 'concept' }] }],
      title: 'Broader Terms',
    },
    {
      name: 'narrower',
      type: 'array',
      of: [{ type: 'reference', to: [{ type: 'concept' }] }],
      title: 'Narrower Terms',
    },
  ],
};
