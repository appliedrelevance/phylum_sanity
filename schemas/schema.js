// First, we must import the schema creator
import createSchema from 'part:@sanity/base/schema-creator';

// Then import schema types from any plugins that might expose them
import schemaTypes from 'all:part:@sanity/base/schema-type';

import address from './common/address';
import companySize from './common/companySize';
import competitor from './common/competitor';
import country from './common/country';
import customer from './common/customer';
import deliverable from './project/deliverable';
import detail from './architecture/detail';
import equipment from './architecture/equipment';
import facility from './architecture/facility';
import feature from './project/feature';
import fixture from './architecture/fixture';
import furniture from './architecture/furniture';
import grunt from './slicingpie/grunt';
import gruntfund from './slicingpie/gruntfund';
import meeting from './project/meeting';
import member from './common/member';
import milestone from './project/milestone';
import organization from './common/organization';
import person from './common/person';
import persona from './project/persona';
import phone from './common/phone';
import product from './project/product';
import program from './architecture/program';
import project from './project/project';
import role from './common/role';
import site from './architecture/site';
import story from './project/story';
import subscription from './common/subscription';
import supplier from './common/supplier';
import task from './project/task';
import venue from './common/venue';

// Then we give our schema to the builder and provide the result to Sanity
export default createSchema({
  // We name our schema
  name: 'default',
  // Then proceed to concatenate our document type
  // to the ones provided by any plugins that are installed
  types: schemaTypes.concat([
    address,
    companySize,
    competitor,
    country,
    customer,
    deliverable,
    detail,
    equipment,
    facility,
    feature,
    fixture,
    furniture,
    grunt,
    gruntfund,
    meeting,
    member,
    milestone,
    organization,
    person,
    persona,
    phone,
    product,
    program,
    project,
    role,
    site,
    story,
    subscription,
    supplier,
    task,
    venue,
  ]),
});
