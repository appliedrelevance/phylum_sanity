export default {
  name: 'equipment',
  title: 'Equipment',
  type: 'object',
  fields: [
    { name: 'name', title: 'Equipment Name', type: 'string' },
    {
      name: 'manufacturer',
      title: 'Manufacturer',
      type: 'reference',
      to: [{ type: 'organization' }],
    },
    { name: 'photos', title: 'Photos', type: 'array', of: [{ type: 'image' }] },
  ],
};
