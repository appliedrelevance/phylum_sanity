export default {
  name: 'program',
  type: 'document',
  title: 'Program',
  description:
    'Architectural Program describes a room or feature of a building',
  fields: [
    { name: 'name', type: 'string', title: 'Program Name' },
    { name: 'details', type: 'array', of: [{ type: 'detail' }] },
  ],
};
