export default {
  name: 'site',
  type: 'document',
  title: 'Site',
  fields: [
    { name: 'name', type: 'string', title: 'Site Name' },
    {
      name: 'description',
      title: 'Site Description',
      type: 'array',
      of: [{ type: 'block' }],
    },
    {
      name: 'facilities',
      title: 'Facilities on this Site',
      type: 'array',
      of: [
        {
          name: 'facilityref',
          title: 'Facility Reference',
          type: 'reference',
          to: [{ type: 'facility' }],
        },
      ],
    },
  ],
};
