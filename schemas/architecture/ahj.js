export default {
  name: 'ahj',
  class: 'object',
  title: 'Agency Having Jurisdiction',
  fields: [
    {
      name: 'organization',
      type: 'organization',
      title: 'Agency Organization',
    },
  ],
};
