export default {
  name: 'codebook',
  title: 'Regulatory Code Book - Collection of Codes',
  type: 'document',
  fields: [
    {
      name: 'name',
      type: 'string',
      title: 'Codebook Name',
    },
    {
      name: 'title',
      type: 'string',
      title: 'Codebook Formal Title',
    },
    {
      name: 'frontMatter',
      title: 'Front Matter',
      type: 'array',
      of: [{ type: 'block' }],
      },
      {
          name: 'section',
          title: 'Section',
    }
  ],
};
