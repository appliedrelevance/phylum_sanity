export default {
  name: 'detail',
  type: 'document',
  title: 'Detail',
  fields: [
    { name: 'name', title: 'Detail Name', type: 'string' },
    {
      name: 'ffe',
      title: 'Furniture, Fixtures and Equipment',
      type: 'array',
      of: [{ type: 'furniture' }, { type: 'fixture' }, { type: 'equipment' }],
    },
  ],
};
