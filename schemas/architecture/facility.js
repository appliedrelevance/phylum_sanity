export default {
  name: 'facility',
  type: 'document',
  title: 'Facility',
  fields: [
    { name: 'name', type: 'string', title: 'Facility Name' },
    {
      name: 'description',
      type: 'array',
      title: 'Facility Description',
      of: [{ type: 'block' }],
    },
    { name: 'poster', type: 'image', title: 'Poster Image' },
    { name: 'address', type: 'address', title: 'Facility Location' },
  ],
};
