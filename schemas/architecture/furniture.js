export default {
  name: 'furniture',
  title: 'Furniture',
  type: 'object',
  fields: [
    { name: 'name', title: 'furniture Name', type: 'string' },
    {
      name: 'manufacturer',
      title: 'Manufacturer',
      type: 'reference',
      to: [{ type: 'organization' }],
    },
    { name: 'photos', title: 'Photos', type: 'array', of: [{ type: 'image' }] },
  ],
};
