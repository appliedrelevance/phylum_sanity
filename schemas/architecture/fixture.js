export default {
  name: 'fixture',
  title: 'Fixture',
  type: 'object',
  fields: [
    { name: 'name', title: 'Fixture Name', type: 'string' },
    {
      name: 'manufacturer',
      title: 'Manufacturer',
      type: 'reference',
      to: [{ type: 'organization' }],
    },
    { name: 'photos', title: 'Photos', type: 'array', of: [{ type: 'image' }] },
  ],
};
