export default {
  name: 'task',
  type: 'document',
  title: 'Task',
  fields: [
    { name: 'name', type: 'string', title: 'Task ID' },
    { name: 'title', type: 'string', title: 'Task Title' },
    { name: 'start', type: 'datetime', title: 'Start' },
    { name: 'finish', type: 'datetime', title: 'Finish' },
    { name: 'duration', type: 'number', title: 'Duration (hours)' },
    {
      name: 'narrative',
      type: 'array',
      title: 'Task Narrative',
      of: [{ type: 'block' }],
    },
    {
      name: 'subtasks',
      type: 'array',
      of: [{ type: 'reference', to: [{ type: 'task' }] }],
    },
    {
      name: 'dependencies',
      title: 'Task Dependencies',
      type: 'array',
      of: [{ type: 'reference', to: [{ type: 'task' }] }],
    },
    {
      name: 'effort',
      title: 'Effort (hours)',
      type: 'number',
    },
    {
      name: 'assignedTo',
      type: 'array',
      of: [{ type: 'reference', to: [{ type: 'member' }] }],
    },
  ],
};
