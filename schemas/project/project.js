export default {
  name: 'project',
  type: 'document',
  title: 'Project',
  fields: [
    { name: 'title', type: 'string', title: 'Project Title' },
    { name: 'name', type: 'string', title: 'Project Name' },
    {
      name: 'slug',
      type: 'slug',
      title: 'Project ID',
      options: {
        source: 'title',
        maxLength: 3,
        slugify: (input) =>
          input
            .split(/\s/)
            .map((trm) => trm.charAt(0))
            .join('')
            .toUpperCase(),
      },
    },
    {
      name: 'tasks',
      type: 'array',
      of: [
        {
          name: 'taskref',
          type: 'reference',
          to: [{ type: 'task' }],
        },
        { name: 'projectref', type: 'reference', to: [{ type: 'project' }] },
        { type: 'task' },
      ],
    },
  ],
};
