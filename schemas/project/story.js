export default {
  name: 'story',
  type: 'document',
  title: 'User Story',
  fields: [
    { name: 'title', type: 'string', title: 'Story Title' },
    { name: 'who', type: 'reference', to: [{ type: 'persona' }] },
    {
      name: 'what',
      type: 'text',
      title: 'What do you want?',
    },
    { name: 'why', type: 'text', title: 'Why do you want it?' },
  ],
};
