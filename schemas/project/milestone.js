export default {
  name: 'milestone',
  type: 'document',
  title: 'Milestone',
  fields: [
    { name: 'name', type: 'string', title: 'Milestone Name' },
    {
      name: 'description',
      type: 'array',
      title: 'Milestone Description',
      of: [{ type: 'block' }],
    },
    {
      name: 'dueAt',
      type: 'datetime',
      title: 'Due Date',
    },
    {
      name: 'dependencies',
      type: 'array',
      title: 'Depends On',
      of: [
        {
          type: 'reference',
          to: [{ type: 'task' }, { type: 'project' }, { type: 'milestone' }],
        },
      ],
    },
  ],
};
