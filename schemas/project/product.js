export default {
  name: 'product',
  type: 'object',
  title: 'Product',
  fields: [
    { name: 'title', type: 'string', title: 'Product Name' },
    {
      name: 'pitch',
      type: 'text',
      title: 'Product Pitch or Marketing Description',
    },
    {
      name: 'screenshots',
      type: 'array',
      title: 'Product Screenshots',
      of: [{ type: 'image' }],
    },
    {
      name: 'features',
      type: 'array',
      title: 'Product Features',
      of: [
        {
          name: 'productFeature',
          type: 'object',
          title: 'Product Feature',
          fields: [
            {
              name: 'featureRef',
              type: 'reference',
              to: [{ type: 'feature' }],
              title: 'Feature',
            },
            {
              name: 'supportLevel',
              type: 'number',
              title: 'Support Level (0-5)',
              options: { list: [0, 1, 2, 3, 4, 5] },
              validation: (Rule) =>
                Rule.min(0).max(5).integer().required().positive(),
            },
            {
              name: 'screenshots',
              type: 'array',
              title: 'Feature Screenshots',
              of: [{ type: 'image' }],
            },
          ],
        },
      ],
    },
  ],
};
