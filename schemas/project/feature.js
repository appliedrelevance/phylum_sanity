export default {
  name: 'feature',
  type: 'document',
  title: 'Feature',
  description: 'Describe the product feature.',
  fields: [
    { name: 'title', type: 'string', title: 'Feature Title' },
    { name: 'description', type: 'text', title: 'Feature Description' },
    {
      name: 'importance',
      type: 'number',
      title: 'Feature Importance',
      options: {
        list: [
          { value: 0, title: '0. Trivial' },
          { value: 1, title: '1. Nice' },
          { value: 2, title: '2. Required' },
          { value: 3, title: '3. Crucial' },
        ],
      },
    },
  ],
};
