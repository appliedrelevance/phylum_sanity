export default {
  name: 'meeting',
  type: 'document',
  title: 'Meeting',
  fields: [
    { name: 'task', type: 'task', title: 'Meeting Task' },
    { name: 'attendees', type: 'reference', to: [{ type: 'member' }] },
    { name: 'venue', type: 'reference', to: [{type: 'venue'}] },
  ],
};
