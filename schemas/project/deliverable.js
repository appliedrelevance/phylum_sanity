export default {
  name: 'deliverable',
  type: 'document',
  title: 'Deliverable',
  fields: [
    { name: 'name', title: 'Deliverable Name', type: 'string' },
    {
      name: 'project',
      title: 'Associated Project',
      type: 'reference',
      to: [{ type: 'project' }],
    },
    { name: 'content', type: 'array', of: [{ type: 'block' }] },
  ],
};
