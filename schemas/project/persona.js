export default {
  name: 'persona',
  type: 'document',
  title: 'Persona',
  description:
    'A persona is an archetype of a person who will use the product or service.',
  fields: [
    { name: 'name', type: 'string', title: 'Persona Name' },
    { name: 'photo', type: 'image', title: 'Persona Photo' },
    { name: 'role', type: 'string', title: 'Persona Role Name' },
    {
      name: 'biography',
      type: 'text',
      title: 'Demographic and Background Information',
    },
    { name: 'goals', type: 'text', title: 'Goals and Motivations' },
    { name: 'minAge', type: 'number', title: 'Persona Minimum Age' },
    { name: 'maxAge', type: 'number', title: 'Persona Maximum Age' },
  ],
};
