export default {
  name: 'phone',
  title: 'Telephone Number',
  type: 'object',
  fields: [
    { name: 'countryCode', title: 'Country Code', type: 'string' },
    { name: 'number', title: 'Phone Number', type: 'string' },
  ],
};
