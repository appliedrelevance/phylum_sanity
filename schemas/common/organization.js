export default {
  name: 'organization',
  type: 'object',
  title: 'Organization',
  description: 'Corporation or other non-human legal entity',
  fields: [
    { name: 'name', title: 'Organization Name', type: 'string' },
    { name: 'logo', title: 'Organization Logo', type: 'image' },
    { name: 'homepage', title: 'Home Page', type: 'url' },
    {
      name: 'primaryContact',
      title: 'Primary Contact',
      type: 'reference',
      to: [{ type: 'person' }],
    },
    {
      name: 'associates',
      title: 'People Associated with the Organization',
      type: 'array',
      of: [{ type: 'reference', to: [{ type: 'person' }] }],
    },
  ],
};
