export default {
  name: 'address',
  type: 'object',
  title: 'Postal Address',
  fields: [
    { name: 'label', type: 'string', title: 'Address Label' },
    { name: 'street', type: 'string', title: 'Street Name and Number' },
    { name: 'mailstop', type: 'string', title: 'Mail Stop or Unit Number' },
    { name: 'city', type: 'string', title: 'City Name' },
    { name: 'state', type: 'string', title: 'State or Province' },
    { name: 'postcode', type: 'string', title: 'Postal Code' },
    {
      name: 'country',
      type: 'reference',
      to: [{ type: 'country' }],
      title: 'Country',
    },
    { name: 'location', type: 'geopoint', title: 'Map Location' },
  ],
};
