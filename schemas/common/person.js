export default {
  name: 'person',
  type: 'document',
  title: 'Person',
  fields: [
    { name: 'fullname', type: 'string', title: 'Full Name' },
    { name: 'portrait', type: 'image', title: 'Portrait' },
    { name: 'description', type: 'text', title: 'Description' },
    {
      name: 'addresses',
      type: 'array',
      title: 'Addresses',
      of: [{ type: 'address' }],
    },
    {
      name: 'phones',
      title: 'Phone Numbers',
      type: 'array',
      of: [{ type: 'phone' }],
    },
    {
      name: 'socials',
      type: 'array',
      title: 'Social Media Links',
      of: [
        {
          name: 'social',
          type: 'object',
          title: 'Social Media Link',
          fields: [
            {
              name: 'platform',
              type: 'string',
              title: 'Platform',
              options: {
                list: [
                  { title: 'Facebook', value: 'facebook' },
                  { title: 'Instagram', value: 'instagram' },
                  { title: 'LinkedIn', value: 'linkedin' },
                ],
              },
            },
            { name: 'link', type: 'string', title: 'Link' },
          ],
        },
      ],
    },
  ],
};
