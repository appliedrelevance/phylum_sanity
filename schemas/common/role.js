export default {
  name: 'role',
  title: 'Role',
  type: 'document',
  fields: [
    {
      name: 'name',
      title: 'Role Short Identifier',
      type: 'string',
      options: {},
      validation: (Rule) => [
        Rule.required()
          .min(2)
          .error(
            `Short Idenitifier is required and must be more than 2 characters.`
          ),
        Rule.max(32).error(`Role identifier must be fewer than 32 characters`),
      ],
    },
    { name: 'title', title: 'Role Name', type: 'string' },
  ],
};
