export default {
  name: 'companySize',
  type: 'object',
  title: 'Company Size',
  fields: [
    {
      name: 'minEmployees',
      type: 'number',
      title: 'Low range of employee count',
    },
    {
      name: 'maxEmployees',
      type: 'number',
      title: 'High range of employee count',
    },
    { name: 'marketCap', type: 'number', title: 'Market capitalization' },
    { name: 'revenue', type: 'number', title: 'Annual Revenue' },
    { name: 'revenuePeriod', type: 'date', title: 'Revenue Period' },
  ],
};
