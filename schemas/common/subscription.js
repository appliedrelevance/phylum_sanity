export default {
  name: 'subscription',
  title: 'Subscription Plan',
  type: 'object',
  fields: [
    { name: 'name', title: 'Subscription Plan Name', type: 'string' },
    { name: 'tagline', title: 'Brief Description of Plan', type: 'string' },
    {
      name: 'features',
      type: 'array',
      title: 'Plan Features',
      of: [
        { type: 'feature' },
        { type: 'reference', to: [{ type: 'feature' }] },
      ],
    },
    {
      name: 'period',
      title: 'Subscription Period',
      type: 'string',
      options: {
        list: [
          { title: 'Monthly', value: 'monthly' },
          { title: 'Forever', value: 'forever' },
          { title: 'Annually', value: 'annually' },
          { title: 'Weekly', value: 'weekly' },
          { title: 'Daily', value: 'hourly' },
          { title: 'Transaction', value: 'transaction' },
          { title: 'Hourly', value: 'hourly' },
        ],
      },
    },
    { name: 'price', title: 'Price per period', type: 'number' },
  ],
};
