export default {
  name: 'supplier',
  type: 'document',
  title: 'Supplier',
  fields: [
    {
      name: 'orgnization',
      title: 'Supplier Organization',
      type: 'organization',
    },
  ],
  preview: {
    select: {
      title: 'organization.name',
      subtitle: 'Subtitle',
      media: 'organization.logo',
    },
    prepare(selection) {
      const { title, media } = selection;
      return {
        title: title,
        media: media,
      };
    },
  },
};
