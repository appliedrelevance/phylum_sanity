export default {
  name: 'venue',
  type: 'document',
  title: 'Venue',
  fields: [
    {
      name: 'name',
      type: 'string',
      title: 'Venue name',
    },
    {
      name: 'address',
      type: 'address',
      title: 'Venue address',
    },
  ],
};
