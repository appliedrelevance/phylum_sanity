export default {
  name: 'customer',
  type: 'document',
  title: 'Customer',
  fields: [
    {
      name: 'organization',
      type: 'organization',
      title: 'Organization',
    },
  ],
  preview: {
    select: {
      title: 'organization.name',
      subtitle: 'Subtitle',
      media: 'organization.logo',
    },
    prepare(selection) {
      const { title, media } = selection;
      return {
        title: title,
        media: media,
      };
    },
  },
};
