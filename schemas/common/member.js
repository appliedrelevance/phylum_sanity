export default {
  name: 'member',
  type: 'document',
  title: 'Member',
  fields: [
    {
      name: 'profile',
      type: 'reference',
      title: 'Profile',
      to: [{ type: 'person' }],
    },
    {
      name: 'roles',
      type: 'array',
      of: [{ type: 'reference', to: [{ type: 'role' }] }],
    },
  ],
  preview: {
    select: {
      title: 'profile.fullname',
      subtitle: 'Subtitle',
      media: 'profile.portrait',
    },
    prepare(selection) {
      const { title, media } = selection;
      return {
        title: title,
        media: media,
      };
    },
  },
};
