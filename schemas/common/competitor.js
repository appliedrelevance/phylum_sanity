export default {
  name: 'competitor',
  type: 'document',
  title: 'Competitor',
  fields: [
    {
      name: 'organization',
      type: 'organization',
      title: 'Parent Organization',
    },
    { name: 'mission', type: 'text', title: 'Company Mission' },
    {
      name: 'objectives',
      type: 'array',
      title: 'Company Objectives',
      of: [{ name: 'objective', type: 'string', title: 'Company Objective' }],
    },
    { name: 'companySize', type: 'companySize', title: 'Company Size' },
    {
      name: 'products',
      type: 'array',
      title: 'Company Products',
      of: [{ type: 'product' }],
    },
    {
      name: 'pricing',
      title: 'Pricing Plans',
      type: 'array',
      of: [{ type: 'subscription' }],
    },
  ],
  preview: {
    select: {
      title: 'organization.name',
      subtitle: 'Subtitle',
      media: 'organization.logo',
    },
    prepare(selection) {
      const { title, media } = selection;
      return {
        title: title,
        media: media,
      };
    },
  },
};
