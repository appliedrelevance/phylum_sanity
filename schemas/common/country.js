export default {
  name: 'country',
  title: 'Country',
  type: 'document',
  fields: [
    { name: 'name', title: 'Country Name', type: 'string' },
    {
      name: 'iso2',
      title: 'ISO2 Code',
      type: 'string',
      description: 'Two character ISO code, e.g., US, CA, UK, RU',
    },
    { name: 'flag', title: 'Country Flag', type: 'image' },
  ],
};
